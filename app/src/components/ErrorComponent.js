import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";

const styles = theme => ({
    paper: {
        padding: 16,
        width: 512,
        textAlign: "center",
        margin: "0 auto",
        marginTop: 16,
        marginBottom: 16
    }
});

const ErrorComponent = function(props) {
    let { classes, message } = props;

    return (
        <Paper className={classes.paper}>
            <Typography variant="h4" gutterBottom>
                Some error happen =(
            </Typography>

            <Typography variant="subtitle1" gutterBottom>
                {message}
            </Typography>
        </Paper>
    );
};

ErrorComponent.propTypes = {
    message: PropTypes.string.isRequired
};

export default withStyles(styles)(ErrorComponent);
