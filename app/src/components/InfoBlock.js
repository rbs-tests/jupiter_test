import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import { Paper, Grid, Link } from "@material-ui/core";
import Avatar from "@material-ui/core/Avatar";
import Divider from "@material-ui/core/Divider";
import Typography from "@material-ui/core/Typography";
import axios from "axios";
import CircularProgress from "@material-ui/core/CircularProgress";
import ErrorComponent from "./ErrorComponent";
import isEqual from "lodash/isEqual";

const styles = theme => ({
    root: {
        width: "100%",
        height: "100%",
        boxSizing: "border-box",
        overflowY: "auto",
        padding: 16
    },
    bigAvatar: {
        marginRight: 10,
        color: "#fff",
        width: 100,
        height: 100
    },
    section: {
        marginTop: 8,
        marginBottom: 8
    },
    progress: {
        margin: theme.spacing.unit * 4
    }
});

class InfoBlock extends Component {
    constructor(props) {
        super(props);

        this.state = {
            person: this.props.person,
            error: null,
            isFetching: true,
            fetched: false
        };

        this.stringToColor = this.stringToColor.bind(this);
    }

    componentDidMount() {
        this.fetch(this.state.person);
    }

    componentDidUpdate(prevProps) {
        if (!isEqual(this.props, prevProps)) {
            this.fetch(this.props.person);
        }
    }

    stringToColor(string) {
        let hash = 0;
        let i;

        /* eslint-disable no-bitwise */
        for (i = 0; i < string.length; i += 1) {
            hash = string.charCodeAt(i) + ((hash << 5) - hash);
        }

        let colour = "#";

        for (i = 0; i < 3; i += 1) {
            const value = (hash >> (i * 8)) & 0xff;
            colour += `00${value.toString(16)}`.substr(-2);
        }
        /* eslint-enable no-bitwise */

        return colour;
    }

    fetch(person) {
        if (person) {
            this.setState({
                person: person,
                error: null,
                isFetching: true,
                fetched: false
            });
            axios
                .get("http://localhost:5000/api/people/" + person.id)
                .then(data => {
                    console.log(data);
                    this.setState({
                        fetched: true,
                        isFetching: false,
                        error: null,
                        person: data.data.person
                    });
                })
                .catch(e => {
                    console.log(e);
                    this.setState({
                        fetched: false,
                        isFetching: false,
                        error: "Some error happen while data fetching"
                    });
                });
        }
    }

    render() {
        let { classes } = this.props;

        const { error, isFetching, fetched, person } = this.state;

       

        if (error) {
            return <ErrorComponent message={error.message} />;
        }

        if (isFetching) {
            return (
                <div style={{ textAlign: "center" }}>
                    <CircularProgress className={classes.progress} />
                </div>
            );
        }

        if (fetched) {
            let color = this.stringToColor(person.name.charAt(0) + "_" + Math.floor(Math.random() * 10001));
            return (
                <Paper className={classes.root}>
                    <Grid container>
                        <Grid item xs={12} md={8} lg={6}>
                            <Grid container className={classes.section}>
                                <Grid item xs={3}>
                                    <Avatar
                                        alt={person.name}
                                        className={classes.bigAvatar}
                                        style={{backgroundColor: color}}
                                    >
                                        {person.name.charAt(0)}
                                    </Avatar>
                                </Grid>
                                <Grid item xs={9}>
                                    <div>
                                        <Typography variant="h4">
                                            {person.name}
                                        </Typography>
                                    </div>
                                    <div>
                                        <Typography variant="body1">
                                            {person.title}
                                        </Typography>
                                    </div>
                                    <div>
                                        <Typography variant="body1">
                                            {person.phone}
                                        </Typography>
                                    </div>
                                    <div>
                                        <Typography>
                                            <Link
                                                href={`mailto:${person.email}`}
                                            >
                                                {person.email}
                                            </Link>
                                        </Typography>
                                    </div>
                                </Grid>
                            </Grid>
                            <Divider />
                            <Grid container className={classes.section}>
                                <Grid item xs={12}>
                                    <div>
                                        <div>
                                            <Typography variant="h5">
                                                Education
                                            </Typography>
                                        </div>
                                        <div>
                                            {person.education.map(
                                                (e, index) => {
                                                    return (
                                                        <Grid
                                                            container
                                                            key={index}
                                                            className={
                                                                classes.section
                                                            }
                                                        >
                                                            <Grid item xs={3}>
                                                                <Typography variant="subtitle2">
                                                                    {
                                                                        e.startYear
                                                                    }{" "}
                                                                    -{" "}
                                                                    {e.endYear ||
                                                                        "Present"}
                                                                </Typography>
                                                            </Grid>
                                                            <Grid item xs={9}>
                                                                <div>
                                                                    <Typography variant="body1">
                                                                        {
                                                                            e.institution
                                                                        }
                                                                    </Typography>
                                                                </div>
                                                                <div>
                                                                    <Typography variant="body1">
                                                                        {
                                                                            e.title
                                                                        }
                                                                    </Typography>
                                                                </div>
                                                            </Grid>
                                                        </Grid>
                                                    );
                                                }
                                            )}
                                        </div>
                                    </div>
                                </Grid>
                            </Grid>
                            <Divider />
                            <Grid container className={classes.section}>
                                <Grid item xs={12}>
                                    <div>
                                        <div>
                                            <Typography variant="h5">
                                                Experience
                                            </Typography>
                                        </div>
                                        <div>
                                            {person.workExperience.map(
                                                (e, index) => {
                                                    return (
                                                        <Grid
                                                            container
                                                            key={index}
                                                            className={
                                                                classes.section
                                                            }
                                                        >
                                                            <Grid item xs={3}>
                                                                <Typography variant="subtitle2">
                                                                    {
                                                                        e.startYear
                                                                    }{" "}
                                                                    -{" "}
                                                                    {e.endYear ||
                                                                        "Present"}
                                                                </Typography>
                                                            </Grid>
                                                            <Grid item xs={9}>
                                                                <div>
                                                                    <Typography variant="body1">
                                                                        {
                                                                            e.institution
                                                                        }
                                                                    </Typography>
                                                                </div>
                                                                <div>
                                                                    <Typography variant="body1">
                                                                        {
                                                                            e.degree
                                                                        }
                                                                    </Typography>
                                                                </div>
                                                            </Grid>
                                                        </Grid>
                                                    );
                                                }
                                            )}
                                        </div>
                                    </div>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </Paper>
            );
        }

        return null;
    }
}

export default withStyles(styles, { withTheme: true })(InfoBlock);
