import React, { Component } from "react";
import PropTypes from 'prop-types';
import { withStyles } from "@material-ui/core/styles";
import { Paper } from "@material-ui/core";
import findIndex from "lodash/findIndex";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Divider from "@material-ui/core/Divider";
import Typography from "@material-ui/core/Typography";

const styles = {
    root: {
        width: "100%",
        height: "100%",
        boxSizing: "border-box",
        overflowY: "auto",
        padding: 16
    },
    section: {
        marginTop: 8
    }
};

class ListBlock extends Component {
    render() {
        let { classes, people, onSelect } = this.props;

        let tree = [];
        for (let i = 0; i < people.length; i++) {
            let letter = people[i].name.charAt(0);

            let index = findIndex(tree, t => {
                return t.letter === letter;
            });

            if (index === -1) {
                tree.push({ letter, arr: [people[i]] });
            } else {
                tree[index].arr.push(people[i]);
            }
        }

        return (
            <Paper className={classes.root}>
                {tree.map((block, indexOuter) => {
                    return (
                        <div key={indexOuter} className={classes.section}>
                            <Typography variant="h6">
                                {block.letter.toUpperCase()}
                            </Typography>
                            <List component="div">
                                {block.arr.map((person, index) => {
                                    return (
                                        <ListItem onClick={() => onSelect(person.id)}key={index} button>
                                            <Typography variant="subtitle1">
                                                {" "}
                                                {person.name}
                                            </Typography>
                                        </ListItem>
                                    );
                                })}
                            </List>
                            <Divider />
                        </div>
                    );
                })}
            </Paper>
        );
    }
}

ListBlock.propTypes = {
    people: PropTypes.array.isRequired,
    onSelect: PropTypes.func.isRequired
}

export default withStyles(styles)(ListBlock);
