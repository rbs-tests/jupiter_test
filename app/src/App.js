import "./App.css";
import React, { Component, Fragment } from "react";
import Grid from "@material-ui/core/Grid";
import CssBaseline from "@material-ui/core/CssBaseline";
import ListBlock from "./components/ListBlock";
import { withStyles } from "@material-ui/core/styles";
import InfoBlock from "./components/InfoBlock";
import axios from "axios";
import CircularProgress from "@material-ui/core/CircularProgress";
import ErrorComponent from "./components/ErrorComponent";
import findIndex from "lodash/findIndex";

const styles = theme => ({
    mainBlock: {
        width: "100%",
        height: "100vh",
        display: "flex",
        backgroundColor: "#eeeeee",
        padding: 16,
        boxSizing: "border-box"
    },
    sideBlock: {},
    progress: {
		margin: theme.spacing.unit * 4,
    }
});

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            people: [],
            error: null,
            isFetching: true,
            fetched: false,
            person: null
        };

        this.onSelect = this.onSelect.bind(this);
    }

    onSelect(id) {
		if(this.state.person && this.state.person.id === id){
			return;
		}
        let index = findIndex(this.state.people, p => p.id === id);
        this.setState({ person: this.state.people[index] });
    }

    componentDidMount() {
        axios
            .get("http://localhost:5000/api/people")
            .then(data => {
                console.log(data);
                this.setState({
                    fetched: true,
                    isFetching: false,
                    error: null,
                    people: data.data.people,
                    person: data.data.people[0]
                });
            })
            .catch(e => {
                console.log(e);
                this.setState({
                    fetched: false,
                    isFetching: false,
                    error: "Some error happen while data fetching"
                });
            });
    }

    render() {
        const { classes } = this.props;
        const { error, isFetching, fetched, people, person } = this.state;

        console.log(people);
        if (error) {
            return <ErrorComponent message={error.message} />;
        }

        if (isFetching) {
            return <div style={{textAlign: "center"}}><CircularProgress className={classes.progress} /></div>;
        }

        if (fetched) {
            return (
                <Fragment>
                    <CssBaseline />
                    <Grid container className={classes.mainBlock}>
                        <Grid item xs={3} className={classes.sideBlock}>
                            <ListBlock
                                onSelect={this.onSelect}
                                people={people}
                            />
                        </Grid>
                        <Grid item xs={9} className={classes.sideBlock}>
                            <InfoBlock person={person} />
                        </Grid>
                    </Grid>
                </Fragment>
            );
        }
        return null;
    }
}

export default withStyles(styles, { withTheme: true })(App);
