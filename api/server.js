require("colors");
const path = require("path");
const express = require("express");

const peopleBlob = require(path.join(__dirname, "data/people.json"));
const people = peopleBlob["people"];

const app = express();

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content-Type, Accept"
    );
    next();
});

app.get("/api/people/:id", getPersonById);
app.get("/api/people", getAllPeople);

const HTTP_PORT = 5000;
app.listen(HTTP_PORT, function(err) {
    if (err) {
        throw err;
    }

    console.log(("HTTP server listening on port " + HTTP_PORT).green);
    console.log(
        "People data:".bold + " http://localhost:" + HTTP_PORT + "/api/people"
    );
});

function getPersonById(req, res) {
    const id = parseInt(req.params.id, 10);
    const filtered = people.filter(function(person) {
        return person.id === id;
    });

    if (filtered.length === 0) {
        res.sendStatus(404);
    } else {
        setTimeout(() => {
            res.json({
                person: filtered[0]
            });
        }, 1000);
    }
}

function getAllPeople(req, res) {
    setTimeout(() => {
        res.json(peopleBlob);
    }, 1000);
}
